# Todo list application

## Original application code taken from

```https://github.com/thobalose/todo-list-app.git 
```

## Sonarcloud Badge

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=sh-opendevops_todo-list-app&metric=alert_status)](https://sonarcloud.io/dashboard?id=sh-opendevops_todo-list-app)